package me.savannuh.hacks;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private Menu menu;
	
	public void onEnable() {
		
		menu = new Menu(this);
		Bukkit.getServer().getPluginManager().registerEvents(new hackMenu(), this);

		
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		Player player = (Player) sender;
		
		if(cmd.getName().equalsIgnoreCase("hacks") && (player.isOp() || player.hasPermission("perm.hack"))) {
					
			menu.open(player);
			return true;
			
		}		
		return true;
	}		
	
}