package me.savannuh.hacks;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;

public class Menu implements Listener {

	public static Inventory inventoryMenu;
	public static ItemStack fly;
	public static ItemStack antiKnockback;
	public static ItemMeta flyMeta;
	public static ItemMeta antiKnockbackMeta;
	
	public Menu(Plugin plugin) {
		
		inventoryMenu = Bukkit.getServer().createInventory(null, 27, ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "Hacks");
		
		ItemStack fly = new ItemStack(Material.ELYTRA);
		ItemMeta flyMeta = fly.getItemMeta();
		flyMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "Fly");
		flyMeta.setLore(Arrays.asList(ChatColor.GOLD + "Start flying"));
		fly.setItemMeta(flyMeta);
		
		ItemStack antiKnockback = new ItemStack(Material.SHIELD);
		ItemMeta antiKnockbackMeta = antiKnockback.getItemMeta();
		antiKnockbackMeta.setDisplayName(ChatColor.AQUA + "" + ChatColor.BOLD + "AntiKnockback");
		antiKnockbackMeta.setLore(Arrays.asList(ChatColor.GOLD + "Don't get any knockback"));
		antiKnockback.setItemMeta(antiKnockbackMeta);
		
		inventoryMenu.setItem(11, fly);
		inventoryMenu.setItem(15, antiKnockback);
		
		Bukkit.getServer().getPluginManager().registerEvents(this, plugin);
		
	}

	public void open(Player p) {
		
		p.openInventory(inventoryMenu);
		
	}
}
