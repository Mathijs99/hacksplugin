package me.savannuh.hacks;

import java.util.ArrayList;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

public class hackMenu implements Listener {
	
    ArrayList<UUID> antiKnockbackList = new ArrayList<UUID>();
    
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e) {
		
		if(e.getEntity() instanceof Player) {
			
			Player target = (Player) e.getEntity();
			
			if(antiKnockbackList.contains(target.getUniqueId())) {
				
				e.setCancelled(true);
				target.damage(e.getDamage());
				
			}		
		}	
	}
	
	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		
		if(!e.getInventory().getName().equalsIgnoreCase(Menu.inventoryMenu.getName()) || e.getCurrentItem().getItemMeta() == null) {
			
			return;
			
		}
		
		Player clicker = (Player) e.getWhoClicked();
		
		if(e.getCurrentItem().getItemMeta().getDisplayName().contains(ChatColor.AQUA + "" + ChatColor.BOLD + "Fly")) {
			
			e.setCancelled(true);
			
			ItemMeta flyMeta = e.getCurrentItem().getItemMeta();
                  
            if(clicker.getAllowFlight() == true) {
            	
            	clicker.setFlying(false);      	
            	clicker.setAllowFlight(false);
            	
            	flyMeta.removeEnchant(Enchantment.FROST_WALKER);
            	e.getCurrentItem().setItemMeta(flyMeta);
            	
            	clicker.sendMessage(ChatColor.YELLOW + "Fly disabled");
            	
            	return;
            	
            }     
            
            clicker.setAllowFlight(true);
            
        	flyMeta.addEnchant(Enchantment.FROST_WALKER, 10, true);   	
        	e.getCurrentItem().setItemMeta(flyMeta);
        	
            clicker.sendMessage(ChatColor.YELLOW + "Fly enabled");
            
		}
		
		if(e.getCurrentItem().getItemMeta().getDisplayName().contains(ChatColor.AQUA + "" + ChatColor.BOLD + "AntiKnockback")) {
			
			e.setCancelled(true);
			
			ItemMeta antiKnockbackMeta = e.getCurrentItem().getItemMeta();
               
            if(antiKnockbackList.contains(clicker.getUniqueId())) {
            	
            	antiKnockbackList.remove(clicker.getUniqueId());
            	
            	antiKnockbackMeta.removeEnchant(Enchantment.KNOCKBACK);
            	e.getCurrentItem().setItemMeta(antiKnockbackMeta);
            	
            	clicker.sendMessage(ChatColor.YELLOW + "AntiKnockback disabled");
            	
            	return;
            	           	
            }
            
            antiKnockbackList.add(clicker.getUniqueId());
            
            antiKnockbackMeta.addEnchant(Enchantment.KNOCKBACK, 10, true);
            e.getCurrentItem().setItemMeta(antiKnockbackMeta);
               
            clicker.sendMessage(ChatColor.YELLOW + "AntiKnockback enabled");
			
		}
	}
}
